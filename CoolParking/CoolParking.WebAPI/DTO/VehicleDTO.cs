﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.DTO
{
    public class VehicleDTO
    {
        public string Id { get; set; }
        public int VehicleType { get; set; }
        public decimal Balance { get; set; }
        /*        public VehicleDTO(string id, int vehicleType, decimal balance)
                {
                    Id = id;
                    VehicleType = vehicleType;
                    Balance = balance;
                }

                [JsonConstructor]
                public VehicleDTO() { }*/
    }
}
