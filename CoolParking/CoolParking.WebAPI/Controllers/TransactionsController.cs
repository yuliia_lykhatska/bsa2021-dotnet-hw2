﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using CoolParking.WebAPI.DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private readonly IParkingService service;
        private readonly Validator validator;
        public TransactionsController(IParkingService _service, Validator _validator)
        {
            service = _service;
            validator = _validator;
        }

        [HttpGet]
        public ActionResult<List<TransactionInfo>> Last()
        {
            return Ok(service.GetLastParkingTransactions());
        }

        [HttpGet]
        public ActionResult<string> All()
        {
            try
            {
                return Ok(service.ReadFromLog());
            }
            catch
            {
                return NotFound();
            }
        }

        [HttpPut]
        public ActionResult TopUpVehicle(TransactionDTO transaction)
        {
            if (!validator.ValidateVehicleId(transaction.Id)) { return BadRequest(); }
            if (transaction.Sum <= 0) { return BadRequest(); }
            try
            {
                service.TopUpVehicle(transaction.Id, transaction.Sum);
                return Ok();
            }
            catch
            {
                return NotFound();
            }
        }
    }
}
