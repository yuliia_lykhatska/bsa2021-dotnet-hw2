﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        private readonly IParkingService service;

        public ParkingController(IParkingService _service)
        {
            service = _service;
        }

        [HttpGet]
        public ActionResult<decimal> Balance()
        {
            return Ok(service.GetBalance());
        }

        [HttpGet]
        public ActionResult<int> Capacity()
        {
            return Ok(service.GetCapacity());
        }

        [HttpGet]
        public ActionResult<int> FreePlaces()
        {
            return Ok(service.GetFreePlaces());
        }
    }
}
