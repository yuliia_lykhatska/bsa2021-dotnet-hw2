﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.WebAPI.DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private readonly IParkingService service;
        private readonly Validator validator;
        public VehiclesController(IParkingService _service, Validator _validator)
        {
            service = _service;
            validator = _validator;
        }

        [HttpGet]
        public ActionResult<List<VehicleDTO>> ReadAll()
        {
            var vehicles = service.GetVehicles();
            var vehiclesDTO = new List<VehicleDTO>();

            foreach(var vehicle in vehicles)
            {
                vehiclesDTO.Add(new VehicleDTO { Id = vehicle.Id, VehicleType = (int)vehicle.VehicleType, Balance = vehicle.Balance });
            }
            return Ok(vehiclesDTO);
        }

        [HttpGet("{id}")]
        public ActionResult<VehicleDTO> Read(string id)
        {
            if (!validator.ValidateVehicleId(id)) { return BadRequest(); }

            var vehicle = service.GetVehicles().ToList().Find(vehicle => vehicle.Id == id);
            if (vehicle == null) { return NotFound(); }

            var vehicleDTO = new VehicleDTO { Id = vehicle.Id, VehicleType = (int)vehicle.VehicleType, Balance = vehicle.Balance };
            return Ok(vehicleDTO);
        }

        [HttpPost]
        public ActionResult Create(VehicleDTO vehicle)
        {

            try
            {
                var createdVehicle = new Vehicle(vehicle.Id, (VehicleType)vehicle.VehicleType, vehicle.Balance);
                service.AddVehicle(createdVehicle);
                return Created("", createdVehicle);
            }
            catch
            {
                return BadRequest();
            }
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(string id)
        {
            if (!validator.ValidateVehicleId(id)) return BadRequest();
            try
            {
                service.RemoveVehicle(id);
                return NoContent();
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
            catch (InvalidOperationException e)
            {
                return BadRequest(e.Message);
            }
        }

    }
}
