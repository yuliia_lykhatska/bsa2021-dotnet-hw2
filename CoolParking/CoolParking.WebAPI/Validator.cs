﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI
{
    public class Validator
    {
        public bool ValidateVehicleId(string id)
        {
            if (!(id.Length == 10)) { return false; }

            string[] idParts = id.Split('-');

            if (!(idParts.Length == 3)) { return false; }

            if (idParts[0].Length != 2 || idParts[2].Length != 2)
            {
                return false;
            }

            string idLetters = idParts[0] + idParts[2];
            foreach (char letter in idLetters)
            {
                if (!Char.IsLetter(letter) || !Char.IsUpper(letter))
                {
                    return false;
                }
            }

            string idDigits = idParts[1];
            foreach (char digit in idDigits)
            {
                if (!Char.IsDigit(digit))
                {
                    return false;
                }
            }

            return true;
        }
    }
    
}
