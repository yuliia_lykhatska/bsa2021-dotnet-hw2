﻿using CoolParking.BL.Models;
using CoolParking.BL.Services;
using System;
using System.IO;
using System.Reflection;

namespace ConsoleUI
{
    class Program
    {
        private static ParkingService service;
        static void Main(string[] args)
        {
            service = new ParkingService(new TimerService(), new TimerService(), new LogService($@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log"));

            Console.WriteLine("Hello World, cool parking is here!");
            run();
        }

        private static string[] MainCommands =
        {
            "view parking balance", 
            "view last income", 
            "view free places", 
            "view last trasactions", 
            "view trasaction history", 
            "view parked vehicles", 
            "park vehicle", 
            "get parked vehicle", 
            "top up vehicle`s balance"
        };

        private static void run()
        {
            Console.WriteLine("Type number of a command you would like to execute.");
            Console.Write(GetMainMenu());

            string input = Console.ReadLine();
            try
            {
                int command = Convert.ToInt32(input);
                ExecuteCommand(command);
            }
            catch (Exception e)
            {
                Console.WriteLine("Something in elvish, can not read it.");
                Console.WriteLine(e.Message);
                run();
            }
        }

        private static string GetMainMenu()
        {
            string menu = "";
            for(int i = 1; i <= MainCommands.Length; i++)
            {
                menu += $"{i}. {MainCommands[i-1]} \n";
            }
            return menu;
        }

        private static void ParkVehicle()
        {
            Console.WriteLine("Type in vehicle`s id, type and balance separated by space. For example, \"AA-1234-BB Bus 100\"");
            Console.WriteLine("Type \"back\" to go back to main menu");
            string input = Console.ReadLine();
            if(input == "back") { run(); }

            try
            {
                string[] inputSplit = input.Split(" ");
                VehicleType type;
                switch (inputSplit[1].ToLower())
                {
                    case "passengercar":
                        type = VehicleType.PassengerCar;
                        break;
                    case "motorcycle":
                        type = VehicleType.Motorcycle;
                        break;
                    case "bus":
                        type = VehicleType.Bus;
                        break;
                    case "truck":
                        type = VehicleType.Truck;
                        break;
                    default:
                        throw new Exception();

                }
                service.AddVehicle(new Vehicle(inputSplit[0], type, Convert.ToDecimal(inputSplit[2])));
            }
            catch (Exception e)
            {
                Console.WriteLine("Something in elvish, can not read it.");
                Console.WriteLine(e.Message);
                ParkVehicle();
            }            
        }

        private static void GetParkedVehicle()
        {
            Console.WriteLine("Type in id of a vehicle you want to take");
            Console.WriteLine("Type \"back\" to go back to main menu");
            string input = Console.ReadLine();
            if (input == "back") { run(); }

            try 
            {
                service.RemoveVehicle(input);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                GetParkedVehicle();
            }
        }

        private static void TopUpVehicle()
        {
            Console.WriteLine("Type in id of a vehicle you want to tup up and sum separated by space");
            Console.WriteLine("Type \"back\" to go back to main menu");
            string input = Console.ReadLine();
            if (input == "back") { run(); }

            string[] inputSplit = input.Split(" ");

            try
            {
                service.TopUpVehicle(inputSplit[0], Convert.ToDecimal(inputSplit[1]));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                TopUpVehicle();
            }
        }

        private static void ExecuteCommand(int command)
        {
            switch (command)
            {
                case 1:
                    Console.WriteLine($"\nCurrent balance is {service.GetBalance()}\n");
                    run();
                    break;
                case 2:
                    Console.WriteLine($"\nLast income is {service.GetSumOfLastTransactions()}\n");
                    run();
                    break;
                case 3:
                    Console.WriteLine($"\nThere are {service.GetFreePlaces()} free places\n");
                    run();
                    break;
                case 4:
                    Console.Write(service.GetLastParkingTransactionsString());
                    run();
                    break;
                case 5:
                    Console.Write(service.ReadFromLog());
                    run();
                    break;
                case 6:
                    Console.Write(service.GetVehiclesString());
                    run();
                    break;
                case 7:
                    ParkVehicle();
                    run();
                    break;
                case 8:
                    GetParkedVehicle();
                    run();
                    break;
                case 9:
                    TopUpVehicle();
                    run();
                    break;
                default:
                    Console.WriteLine("There is no such command :(");
                    run();
                    break;
            }
        }
    }
}
