﻿using System;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public string Id { get; }
        public VehicleType VehicleType { get; }
        public decimal Balance { get; internal set; }
        public Vehicle(string id, VehicleType type, decimal balance)
        {
            if (!ValidateId(id)) 
            { 
                throw new ArgumentException("ID should be in format \"ХХ-YYYY-XX\", where X is an uppercase letter and Y is a digit. Please try again."); 
            }
            if (!ValidateBalance(balance))
            {
                throw new ArgumentException("Balance should a non-negative number. Please change balance and try again.");
            }
            Id = id;
            VehicleType = type;
            Balance = balance;
        }

        public decimal Price 
        {
            get 
            {
                return Settings.Prices[VehicleType];
            }
        }
        public static string GenerateRandomRegistrationPlateNumber()
        {
            var random = new Random();
            string letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            string generatedId = "";

            generatedId += random.Next(0, letters.Length - 1);
            generatedId += random.Next(0, letters.Length - 1);

            generatedId += '-';

            for(int i = 0; i < 4; i++)
            {
                generatedId += random.Next(0, 9).ToString();
            }

            generatedId += '-';

            generatedId += random.Next(0, letters.Length - 1);
            generatedId += random.Next(0, letters.Length - 1);

            return generatedId;
        }

        private bool ValidateId(string id)
        {
            if (!(id.Length == 10)) { return false; }

            string[] idParts = id.Split('-');

            if (!(idParts.Length == 3)) { return false; }

            if (idParts[0].Length != 2 || idParts[2].Length != 2)
            { 
                return false;
            }

            string idLetters = idParts[0] + idParts[2];
            foreach(char letter in idLetters)
            {
                if(!Char.IsLetter(letter) || !Char.IsUpper(letter))
                {
                    return false;
                }
            }

            string idDigits = idParts[1];
            foreach(char digit in idDigits)
            {
                if (!Char.IsDigit(digit))
                {
                    return false;
                }
            }

            return true;
        }

        private bool ValidateBalance(decimal balance)
        {
            if (balance < 0) { return false; }
            return true;
        }

        public decimal Withdraw()
        {
            decimal debt = Balance > Price ? 0 : Balance > 0 ? Price - Balance : Price;
            decimal withdrawSum = Price - debt + debt * Settings.FineCoefficient;
            Balance -= withdrawSum;
            return withdrawSum;
        }

        public override string ToString()
        {
            return $"\nId:{Id} | Type:{VehicleType} | Balance:{Balance}";
        }
    }
}