﻿using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static decimal ParkingBalance = 0;
        public static int ParkingCapacity = 10;
        public static int PaymentPeriod = 5; //in seconds
        public static int LoggingPeriod = 60; //in seconds
        public static Dictionary<VehicleType, decimal> Prices = new Dictionary<VehicleType, decimal>() 
        {
            { VehicleType.PassengerCar, 2 },
            { VehicleType.Truck, 5 },
            { VehicleType.Bus, 3.5m },
            { VehicleType.Motorcycle, 1 }
        };
        public static decimal FineCoefficient = 2.5m;
    }
}
