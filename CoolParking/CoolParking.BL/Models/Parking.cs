﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        private static Parking instance;
        public decimal Balance { get; set; }
        public List<Vehicle> ParkedVehicles { get; set; }
        public int FreePlaces 
        {
            get { return Settings.ParkingCapacity - ParkedVehicles.Count; }
        }

        public bool IsFull
        {
            get { return FreePlaces == 0; }
        }
        public List<TransactionInfo> Transactions { get; }

        private Parking()
        {
            Balance = 0;
            ParkedVehicles = new List<Vehicle>();
            Transactions = new List<TransactionInfo>();
        }

        public static Parking getInstance()
        {
            if(instance == null)
            {
                instance = new Parking();
            }
            return instance;
        }
        public Vehicle FindVehicle(string vehicleId)
        {
            return ParkedVehicles.Find(vehicle => vehicle.Id == vehicleId);
        }
    }
}
