﻿using System;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public string VehicleID { get; }
        public DateTime Time { get; }
        public decimal Sum { get; }
        public TransactionType Type { get; }

        public TransactionInfo(string vehicleId, DateTime time, decimal sum, TransactionType type)
        {
            VehicleID = vehicleId;
            Time = time;
            Sum = sum;
            Type = type;
        }
        public enum TransactionType
        {
            withdraw,
            topUp
        }

        public override string ToString()
        {
            return $"Vehicle: {VehicleID} | {Type} | {Sum} | {Time}";
        }
    }
}
