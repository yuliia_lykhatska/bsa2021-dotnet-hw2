﻿using System.Diagnostics;
using System.Timers;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        public TimerService()
        {
            Timer = new Timer();
            Timer.Elapsed += TimerElapsedEventHandler;
        }
        public double Interval { get; set; }

        public event ElapsedEventHandler Elapsed;

        public Timer Timer;

        public void TimerElapsedEventHandler(object? sender, ElapsedEventArgs e)
        {
            FireElapsedEvent();
        }

        public void FireElapsedEvent()
        {
            Elapsed?.Invoke(this, null);
        }

        public void Start()
        {
            Timer.Interval = Interval * 1000;
            Timer.Start();
        }

        public void Stop()
        {
            Timer.Stop();
        }

        public void Dispose()
        {
            Timer.Dispose();
        }

    }
}
