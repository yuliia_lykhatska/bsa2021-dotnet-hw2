﻿using CoolParking.BL.Interfaces;
using System;
using System.IO;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public LogService(string logPath)
        {
            LogPath = logPath;
        }
        public string LogPath { get; set; }

        public string Read()
        {
            try
            {
                using (StreamReader r = File.OpenText(LogPath))
                {
                    return r.ReadToEnd();
                }
            }
            catch (FileNotFoundException e)
            {
                throw new InvalidOperationException("Log file does not exist");
            }
        }

        public void Write(string logInfo)
        {
            using (StreamWriter w = File.AppendText(LogPath))
            {
                w.WriteLine(logInfo);
            }

        }
    }
}
