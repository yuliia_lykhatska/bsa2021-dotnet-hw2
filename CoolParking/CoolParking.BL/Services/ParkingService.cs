﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.ObjectModel;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private Parking Parking { get; }
        private ITimerService WithdrawTimer { get; }
        private ITimerService LogTimer { get; }
        private ILogService Logger { get; }
        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logger)
        {
            WithdrawTimer = withdrawTimer;
            WithdrawTimer.Interval = Settings.PaymentPeriod;
            WithdrawTimer.Start();

            LogTimer = logTimer;
            LogTimer.Interval = Settings.LoggingPeriod;
            LogTimer.Start();

            Logger = logger;
            Parking = Parking.getInstance();

            WithdrawTimer.Elapsed += Withdraw;
            LogTimer.Elapsed += Log;
        }
        public void AddVehicle(Vehicle vehicle)
        {
            if (Parking.IsFull) { throw new InvalidOperationException("Parking is full"); }
            if (Parking.FindVehicle(vehicle.Id) != null) { throw new ArgumentException("Vehicle is already parked"); }
            Parking.ParkedVehicles.Add(vehicle);
        }

        public void Dispose()
        {
            Parking.ParkedVehicles.Clear();
            Parking.Transactions.Clear();
            Parking.Balance = 0;
            Parking.Transactions.Clear();
            WithdrawTimer.Dispose();
            LogTimer.Dispose();
        }

        public decimal GetBalance()
        {
            return Parking.Balance;
        }

        public int GetCapacity()
        {
            return Settings.ParkingCapacity;
        }

        public int GetFreePlaces()
        {
            return Parking.FreePlaces;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return Parking.Transactions.ToArray();
        }

        public decimal GetSumOfLastTransactions()
        {
            var transactions = GetLastParkingTransactions();

            decimal sum = 0;
            foreach(var transaction in transactions)
            {
                sum += transaction.Sum;
            }
            return sum;
        }

        public string GetLastParkingTransactionsString()
        {
            var str = "";
            var transactions = GetLastParkingTransactions();

            foreach(var transaction in transactions)
            {
                str += transaction.ToString();
            }
            return str;
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>(Parking.ParkedVehicles);
        }

        public string GetVehiclesString()
        {
            var vehicles = GetVehicles();
            string str = "";
            foreach(var vehicle in vehicles)
            {
                str += $"\n{vehicle}\n";
            }
            return str;
        }

        public string ReadFromLog()
        {
            return Logger.Read();
        }

        public void RemoveVehicle(string vehicleId)
        {
            var vehicle = Parking.FindVehicle(vehicleId);
            if (vehicle == null) { throw new ArgumentException("Vehicle with such ID is not found."); }
            if (vehicle.Balance < 0) { throw new InvalidOperationException("Can not remove vehicle with negative balance."); } 
            Parking.ParkedVehicles.Remove(vehicle);
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (sum < 0) { throw new ArgumentException("Sum must be a non-negative number."); }
            var vehicle = Parking.FindVehicle(vehicleId);
            if (vehicle == null) { throw new ArgumentException("Vehicle with such ID is not found."); }
            vehicle.Balance += sum;
            //Parking.Transactions.Add(new TransactionInfo(vehicleId, DateTime.Now, sum, TransactionInfo.TransactionType.topUp));
        }

        public void Withdraw(object? sender, ElapsedEventArgs e)
        {
            foreach(var vehicle in Parking.ParkedVehicles)
            {  
                var sum = vehicle.Withdraw();
                Parking.Balance += sum;
                Parking.Transactions.Add(new TransactionInfo(vehicle.Id, DateTime.Now, sum, TransactionInfo.TransactionType.withdraw));
            }
        }

        public void Log(object? sender, ElapsedEventArgs e)
        {
            Logger.Write("");
            foreach(var transaction in Parking.Transactions)
            {
                Logger.Write(transaction.ToString());
            }
            Parking.Transactions.Clear();
        }

        public Vehicle FindVehicle(string id)
        {
            return Parking.FindVehicle(id);
        }
    }
}

